#include "FemtoFitter.h"

//////////////////////////////////////////////////////
FemtoFitter::FemtoFitter() {
  
  // We run the functions in the constructor so that the fitter
  // works even if the parameters are not specified from the outside.
  Set3DFitInitVal();
  Set3DFitInitValErr();
  Set1DFitInitVal();
  Set1DFitInitValErr();
  SetFitParameters();

}

//////////////////////////////////////////////////////
FemtoFitter::~FemtoFitter() {}

//////////////////////////////////////////////////////
void FemtoFitter::Clear() {
  mKCoeffSpherSource.clear();
  mKCoeffCauchSource.clear();
  mParFixNumber.clear();
  mParLimNumber.clear();
  mParStepNumber.clear();
  mFixVar.clear();
  mLimVarLo.clear();
  mLimVarHi.clear();
  mStepVar.clear();
  mGet3DFitPar.clear();
  mGet3DFitParErr.clear();
  mGet1DFitPar.clear();
  mGet1DFitParErr.clear();
  h3DCfProj.clear();
  h3DCfProjFit.clear();
  h1DCf.clear();
  h1DCfFit.clear();
}

//////////////////////////////////////////////////////
void FemtoFitter::CalculateCoulombCorrection(TAxis *xAxis) {
// Calculation Coulomb factor
  vector<double> vector;
  if (mCoulombMethod == 1) vector = mKCoeffCauchSource;
  if (mCoulombMethod == 2) vector = mKCoeffSpherSource;

  if (vector.empty()) {
    if (mCoulombMethod != 0) {
      cout << "Coulomb factor calculation begin" << endl;

      double qLow = xAxis->GetXmin();
      double qHi = xAxis->GetXmax(); 
      double qBins = xAxis->GetNbins();
      double qStep = (qHi-qLow)/qBins;
      double theorQinv = 0;
    
      for (int iQinv = 0; iQinv < qBins*2; iQinv++) {
        theorQinv = qStep/2 + iQinv*qStep;
        if (mCoulombMethod == 2) {
          mKCoeffSpherSource.push_back(LednickyWeigthCalc(theorQinv));
        } // if (mCoulombMethod == 2)
      
        if (mCoulombMethod == 1) {
          // ksi is the Landau parameter
          double ksi = (alpha*mMassPart)/theorQinv;
          // Gamow factor from Z. Phys. 51, 204 (1928).
          double Gamov = (2*M_PI*ksi)/(exp(2*M_PI*ksi) - 1);
          double K = Gamov*(1 + M_PI*ksi*theorQinv*mCoulRad/(1.26 + theorQinv*mCoulRad));
          mKCoeffCauchSource.push_back(K);
        } //if (mCoulombMethod == 1)
      
      } // iQinv
      cout << "Coulomb factor calculation end" << endl;
    } //if (mCoulombMethod != 0)
  }
}

//////////////////////////////////////////////////////
void FemtoFitter::DoFit3D() {
  
  cout << "//////////////////////////////////////////////////////" << endl;
  
  CalculateCoulombCorrection(hQinv3D->GetXaxis());

  cout << "Minimization part begin" << endl;
  // Follow the example: https://root.cern.ch/doc/master/NumericalMinimization_8C.html
  // Create Minuit2 Minimizer with Migrad algo (best of all)
  Minimizer* gMinuit = Factory::CreateMinimizer("Minuit2", "Migrad");
  int mNumberOfFitPar = 0;
  if (mSourceMethod == 0 || mSourceMethod == 1) mNumberOfFitPar = m3DNumParInt;
  if (mSourceMethod == 2) mNumberOfFitPar = m3DNumPar;
  // Create function wrapper for minimizer a IMultiGenFunction type
  // for 8 (or 6 for intHBT) fit parameters (Norm + lambda + 3radii + 3cross-comp.)
  Functor f(&Minimization3DLogLike, mNumberOfFitPar);
  gMinuit->SetFunction(f); // Set function which may be minimized
  // Define parameters and its names for the Minuit 
  string nameVal[m3DNumPar]  = {"Norm", "lambda", "Ro2", "Rs2", "Rl2", "Ros2", "Rol2", "Rsl2"};
  for (int iPar = 0; iPar < mNumberOfFitPar; iPar++) {
    gMinuit->SetVariable(iPar, nameVal[iPar].c_str(),
                         m3DInitVal[iPar], m3DInitValErr[iPar]);
  }
  
  if (mFitterCustomize)
  {
    gMinuit->SetErrorDef(mErrDef); 
    gMinuit->SetMaxFunctionCalls(mMaxCalls); 
    gMinuit->SetPrecision(mPrecision);
    gMinuit->SetTolerance(mTolerance); 
    gMinuit->SetStrategy(mStrategy);
    gMinuit->SetValidError(mValidError);
    for (long unsigned int i = 0; i < mParFixNumber.size(); i++)
    {
      gMinuit->SetFixedVariable(mParFixNumber.at(i), nameVal[mParFixNumber.at(i)].c_str(), mFixVar.at(i));
    }
    for (long unsigned int i = 0; i < mParLimNumber.size(); i++)
    {
      gMinuit->SetVariableLimits(mParLimNumber.at(i), mLimVarLo.at(i), mLimVarHi.at(i));
    }
    for (long unsigned int i = 0; i < mParStepNumber.size(); i++)
    {
      gMinuit->SetVariableStepSize(mParStepNumber.at(i), mStepVar.at(i));
    }
  } //mFitterCustomize
  
  gMinuit->SetPrintLevel(mPrintLevel);

  gMinuit->Minimize();
  if (mValidError) {
    // In order you need really precise errors we need to recalculate error matrix
    // (also SetValidError do it too)
    // but I think it's dosen't matter for femtoscopy
    // read: http://www.fresco.org.uk/minuit/cern/node32.html
    gMinuit->Hesse(); 
    gMinuit->Minimize();
  }
  cout << "Minimization part end" << endl;
  
  // Get parameters wich were recieved in Minuit (for weigth in fit histo)
  const double *m3DFitPar = gMinuit->X();
  const double *m3DFitParErr = gMinuit->Errors();
  for (int iPar = 0; iPar < (int)(gMinuit->NDim()); iPar++) { //NDim return number of parameters which minuit have
    mGet3DFitPar.push_back(m3DFitPar[iPar]);
    mGet3DFitParErr.push_back(m3DFitParErr[iPar]);
  }

  TH3F* hNumFit3D = (TH3F*)hDen3D->Clone("hNumFit3D"); // Create histo for our fit
  TH3F* hRat3D = (TH3F*)hNum3D->Clone("hRat3D"); 
  TH3F* hRatFit3D = (TH3F*)hNumFit3D->Clone("hRatFit3D"); 
  int mNdf3D = 0; // number of degrees of freedom

  m3DChi2PerNdf = 0;
  double q[3];
  for (int iOut = 1; iOut <= hNumFit3D->GetXaxis()->GetNbins(); iOut++) {
    q[0] = hNumFit3D->GetXaxis()->GetBinCenter(iOut);
    
    for (int iSide = 1; iSide <= hNumFit3D->GetYaxis()->GetNbins(); iSide++) {
      q[1] = hNumFit3D->GetYaxis()->GetBinCenter(iSide);
      
      for (int iLong = 1; iLong <= hNumFit3D->GetZaxis()->GetNbins(); iLong++) {
        q[2] = hNumFit3D->GetZaxis()->GetBinCenter(iLong);
        
        mNdf3D++;
        
        // Adding fit weigth to our fit histo
        hNumFit3D->SetBinContent(iOut, iSide, iLong,
                                 hNumFit3D->GetBinContent(iOut, iSide, iLong)*BowlerSinyukovFit(q, m3DFitPar, iOut, iSide, iLong));
        hNumFit3D->SetBinError(iOut, iSide, iLong, 0.);

        //Divide histos by hand to avoid 3 nested loops
        hRat3D->SetBinContent(iOut, iSide, iLong,
                               hNum3D->GetBinContent(iOut, iSide, iLong) /
                               hDen3D->GetBinContent(iOut, iSide, iLong));
        hRat3D->SetBinError(iOut, iSide, iLong,
                             sqrt(pow((1./hDen3D->GetBinContent(iOut, iSide, iLong)) *
                                      hNum3D->GetBinError(iOut, iSide, iLong),2) +
                                  pow((hNum3D->GetBinContent(iOut, iSide, iLong) /
                                       pow(hDen3D->GetBinContent(iOut, iSide, iLong),2)) *
                                      hDen3D->GetBinError(iOut, iSide, iLong),2)));
        hRatFit3D->SetBinContent(iOut, iSide, iLong,
                                  hNumFit3D->GetBinContent(iOut, iSide, iLong) /
                                  hDen3D->GetBinContent(iOut, iSide, iLong));
        hRatFit3D->SetBinError(iOut, iSide, iLong, 0.);

        //Chi2 calculation
        // https://pdg.lbl.gov/2015/reviews/rpp2015-rev-statistics.pdf (38.48)
        double diff = hRat3D->GetBinContent(iOut, iSide, iLong) - hRatFit3D->GetBinContent(iOut, iSide, iLong);
        double sigma = hRat3D->GetBinError(iOut, iSide, iLong);
        if (hNum3D->GetBinContent(iOut, iSide, iLong) == 0 || hDen3D->GetBinContent(iOut, iSide, iLong) == 0) mNdf3D--;
        else m3DChi2PerNdf += diff*diff/(sigma*sigma);
      } // iLong
    } // iSide
  } // iOut
  
  // Scaling our histos
  hNum3D->Scale(1./m3DFitPar[0]);
  hNumFit3D->Scale(1./m3DFitPar[0]);

  for (int iProj = 0; iProj < 3; iProj++) {

    // Make projections for Num, Den and Fit
    TH1F* hNumProj = MakeProjection(iProj, hNum3D);
    TH1F* hDenProj = MakeProjection(iProj, hDen3D);
    TH1F* hNumFitProj = MakeProjection(iProj, hNumFit3D);

    //Make correlation functions
    hNumProj->Divide(hDenProj);
    hNumFitProj->Divide(hDenProj);

    // Writing CF and fits to vectors
    h3DCfProj.push_back((TH1F*)hNumProj->Clone());
    h3DCfProjFit.push_back((TH1F*)hNumFitProj->Clone());
   
  } // iProj
  
  // Display the main parameters.
  // It should be remembered that the main parameters are in squares.
  cout << "Most important femto parameters:" << endl;
  cout << "Norm  = " << m3DFitPar[0]
       << " +/- " << m3DFitParErr[0] << endl;
  cout << "Lam  = " << m3DFitPar[1]
       << " +/- " << m3DFitParErr[1] << endl;
  cout << "Ro  = " << sqrt(m3DFitPar[2])
       << " +/- " << m3DFitParErr[2]/(2.0*sqrt(m3DFitPar[2])) << endl;
  cout << "Rs = " << sqrt(m3DFitPar[3])
       << " +/- " << m3DFitParErr[3]/(2.0*sqrt(m3DFitPar[3])) << endl;
  cout << "Rl = " << sqrt(m3DFitPar[4])
       << " +/- " << m3DFitParErr[4]/(2.0*sqrt(m3DFitPar[4])) << endl;
  cout << "chi2/NDF = " << m3DChi2PerNdf << "/"<< mNdf3D - m3DNumPar - 1 << endl;
  if (mSourceMethod == 0 || mSourceMethod == 1) m3DChi2PerNdf = m3DChi2PerNdf/(mNdf3D - m3DNumPar - 3 - 1);
  if (mSourceMethod == 2) m3DChi2PerNdf = m3DChi2PerNdf/(mNdf3D - m3DNumPar - 1);
  cout << "chi2/NDF = " << m3DChi2PerNdf << endl;
  
  cout << "//////////////////////////////////////////////////////" << endl;

} //DoFit3D

//////////////////////////////////////////////////////
void FemtoFitter::DoFit1D() {
  
  cout << "//////////////////////////////////////////////////////" << endl;
  
  CalculateCoulombCorrection(hNum1D->GetXaxis());

  cout << "Minimization part begin" << endl;
  // Follow the example: https://root.cern.ch/doc/master/NumericalMinimization_8C.html
  // Create Minuit2 Minimizer with Migrad algo (best of all)
  Minimizer* gMinuit = Factory::CreateMinimizer("Minuit2", "Migrad");
  gMinuit->SetMaxFunctionCalls(mMaxCalls); // needs to achieve function minimum
  gMinuit->SetTolerance(mTolerance); // expected distance reached from the minimum
  gMinuit->SetPrintLevel(mPrintLevel); // print information about minimization
  gMinuit->SetErrorDef(mErrDef); // set scale for calculating the errors - DO I NEED THIS?????
  // Create function wrapper for minimizer a IMultiGenFunction type
  // for 3 fit parameters (Norm + lambda + inv rad)
  Functor f(&Minimization1DLogLike, 3);
  gMinuit->SetFunction(f); // Set function which may be minimized
  // Define parameters and its names for the Minuit 
  string nameVal[m1DNumPar]  = {"Norm", "lambda", "Rinv"};
  for (int iPar = 0; iPar < m1DNumPar; iPar++) {
    gMinuit->SetVariable(iPar, nameVal[iPar].c_str(),
                         m1DInitVal[iPar], m1DInitValErr[iPar]);
  }
  
  if (mFitterCustomize)
  {
    gMinuit->SetErrorDef(mErrDef); 
    gMinuit->SetMaxFunctionCalls(mMaxCalls); 
    gMinuit->SetPrecision(mPrecision);
    gMinuit->SetTolerance(mTolerance); 
    gMinuit->SetStrategy(mStrategy);
    gMinuit->SetValidError(mValidError);
    for (long unsigned int i = 0; i < mParFixNumber.size(); i++)
    {
      gMinuit->SetFixedVariable(mParFixNumber.at(i), nameVal[mParFixNumber.at(i)].c_str(), mFixVar.at(i));
    }
    for (long unsigned int i = 0; i < mParLimNumber.size(); i++)
    {
      gMinuit->SetVariableLimits(mParLimNumber.at(i), mLimVarLo.at(i), mLimVarHi.at(i));
    }
    for (long unsigned int i = 0; i < mParStepNumber.size(); i++)
    {
      gMinuit->SetVariableStepSize(mParStepNumber.at(i), mStepVar.at(i));
    }
  } //mFitterCustomize
  
  gMinuit->SetPrintLevel(mPrintLevel);

  gMinuit->Minimize();
  if (mValidError) {
    // In order you need really precise errors we need to recalculate error matrix
    // (also SetValidError do it too)
    // but I think it's dosen't matter for femtoscopy
    // read: http://www.fresco.org.uk/minuit/cern/node32.html
    gMinuit->Hesse(); 
    gMinuit->Minimize();
  }
  
  cout << "Minimization part end" << endl;
  
  // Get parameters wich were recieved in Minuit (for weigth in fit histo)
  const double *m1DFitPar = gMinuit->X();
  const double *m1DFitParErr = gMinuit->Errors();
  for (int iPar = 0; iPar < m1DNumPar; iPar++) {
    mGet1DFitPar.push_back(m1DFitPar[iPar]);
    mGet1DFitParErr.push_back(m1DFitParErr[iPar]);
  }

  TH1F* hNumFit1D = (TH1F*)hDen1D->Clone("hNumFit1D"); // Create histo for our fit
  TH3F* hRat1D = (TH3F*)hNum1D->Clone("hRat1D"); 
  TH3F* hRatFit1D = (TH3F*)hNumFit1D->Clone("hRatFit1D"); 
  int mNdf1D = 0; // number of degrees of freedom
  m1DChi2PerNdf = 0;

  // Adding fit weigth to our fit histo
  for (int iInv = 1; iInv <= hNumFit1D->GetXaxis()->GetNbins(); iInv++) {

    mNdf1D++;
    hNumFit1D->SetBinContent(iInv, hNumFit1D->GetBinContent(iInv)*BowlerSinyukov1DFit(m1DFitPar, iInv));
    hNumFit1D->SetBinError(iInv, 0.);

    //Divide histos by hand to avoid nested loop
    hRat1D->SetBinContent(iInv,
                          hNum1D->GetBinContent(iInv) /
                          hDen1D->GetBinContent(iInv));
    hRat1D->SetBinError(iInv,
                        sqrt(pow((1./hDen1D->GetBinContent(iInv)) *
                                 hNum1D->GetBinError(iInv),2) +
                             pow((hNum1D->GetBinContent(iInv) /
                                  pow(hDen1D->GetBinContent(iInv),2)) *
                                 hDen1D->GetBinError(iInv),2)));
    hRatFit1D->SetBinContent(iInv,
                             hNumFit1D->GetBinContent(iInv) /
                             hDen1D->GetBinContent(iInv));
    hRatFit1D->SetBinError(iInv, 0.);

    //Chi2 calculation
    // https://pdg.lbl.gov/2015/reviews/rpp2015-rev-statistics.pdf (38.48)
    double diff = hRat1D->GetBinContent(iInv) - hRatFit1D->GetBinContent(iInv);
    double sigma = hRat1D->GetBinError(iInv);
    if (hNum1D->GetBinContent(iInv) == 0 || hDen1D->GetBinContent(iInv) == 0) mNdf1D--;
    else m1DChi2PerNdf += diff*diff/(sigma*sigma);
  } // iInv
  
  // Scaling our histos
  hNum1D->Scale(1./m1DFitPar[0]);
  hNumFit1D->Scale(1./m1DFitPar[0]);
  hNum1D->Divide(hDen1D);
  hNumFit1D->Divide(hDen1D);
  
  h1DCf.push_back( hNum1D );
  h1DCfFit.push_back( hNumFit1D );

  // Display the main parameters.
  // It should be remembered that the main parameters are in squares.
  cout << "Most important femto parameters:" << endl;
  cout << "Norm  = " << m1DFitPar[0]
       << " +/- " << m1DFitParErr[0] << endl;
  cout << "Lam  = " << m1DFitPar[1]
       << " +/- " << m1DFitParErr[1] << endl;
  cout << "Rinv  = " << sqrt(m1DFitPar[2])
       << " +/- " << m1DFitParErr[2]/(2.0*sqrt(m1DFitPar[2])) << endl;
  cout << "chi2/NDF = " << m1DChi2PerNdf << "/" << (mNdf1D - m1DNumPar - 1) << endl;
  m1DChi2PerNdf = m1DChi2PerNdf/(mNdf1D - m1DNumPar - 1);
  cout << "chi2/NDF = " << m1DChi2PerNdf << endl;
  
  cout << "//////////////////////////////////////////////////////" << endl;

} //DoFit1D

//////////////////////////////////////////////////////
double FemtoFitter::Minimization3DLogLike(const double *par) {
  double chi2 = 0.;
  double q[3];

  for (int iOut = 1; iOut <= hNum3D->GetXaxis()->GetNbins(); iOut++) {
    q[0] = hNum3D->GetXaxis()->GetBinCenter(iOut);
    if (mFitRange[0] > q[0] || q[0] > mFitRange[1]) continue;

    for (int iSide = 1; iSide <= hNum3D->GetYaxis()->GetNbins(); iSide++) {
      q[1] = hNum3D->GetYaxis()->GetBinCenter(iSide);
      if (mFitRange[0] > q[1] || q[1] > mFitRange[1]) continue;

      for (int iLong = 1; iLong <= hNum3D->GetZaxis()->GetNbins(); iLong++) {
        q[2] = hNum3D->GetZaxis()->GetBinCenter(iLong);
        if (mFitRange[0] > q[2] || q[2] > mFitRange[1]) continue;

        double A = hNum3D->GetBinContent(iOut, iSide, iLong);
        //if (A == 0) continue; //this means num is empty
        double B = hDen3D->GetBinContent(iOut, iSide, iLong);
        //if (B == 0) continue; //this means den is empty
        double C = BowlerSinyukovFit(q, par, iOut, iSide, iLong);
        
        // This minimization function was taken from Phys. Rev. C 66 (2002) 054906
        // https://arxiv.org/pdf/nucl-ex/0204001.pdf
        if (A > 0.) chi2 += A*log( (A+B)*C / (A*(C+1)) );
        if (B > 0.) chi2 += B*log( (A+B) / (B*(C+1)) );

      } //iLong
    } //iSide
  } //iOut
  
  chi2 *= -2.;
  return chi2;
  
} // Minimization3DLogLike

//////////////////////////////////////////////////////
double FemtoFitter::Minimization1DLogLike(const double *par) {
  double chi2 = 0.;
  double qInv;
  
  for (int iInv = 1; iInv <= hNum1D->GetXaxis()->GetNbins(); iInv++) {
    qInv = hNum1D->GetXaxis()->GetBinCenter(iInv);
    if (mFitRange[0] > qInv || qInv > mFitRange[1]) continue;

    double A = hNum1D->GetBinContent(iInv);
    if (A == 0) continue; //this means num is empty
    double B = hDen1D->GetBinContent(iInv);
    if (B == 0) continue; //this means den is empty
    double C = BowlerSinyukov1DFit(par, iInv);

    // This minimization function was taken from Phys. Rev. C 66 (2002) 054906
    // https://arxiv.org/pdf/nucl-ex/0204001.pdf
    if (A > 0.) chi2 += A*log( (A+B)*C / (A*(C+1)) );
    if (B > 0.) chi2 += B*log( (A+B) / (B*(C+1)) );

  } //iOut
  
  chi2 *= -2.;
  return chi2;
  
} // Minimization1DLogLike

//////////////////////////////////////////////////////
TH1F* FemtoFitter::MakeProjection(int osl, TH3F* hToProj){
  
  TAxis *mAxis[3] = {0};
  string mAxisName, mProjName;

  if (osl == 0)
  {
    mAxis[0] = hToProj->GetXaxis();
    mAxis[1] = hToProj->GetYaxis();
    mAxis[2] = hToProj->GetZaxis();
    mAxisName = "x";  mProjName = "o";
  }

  if (osl == 1)
  {
    mAxis[0] = hToProj->GetYaxis();
    mAxis[1] = hToProj->GetZaxis();
    mAxis[2] = hToProj->GetXaxis();
    mAxisName = "y";  mProjName = "s";
  }

  if (osl == 2)
  {
    mAxis[0] = hToProj->GetZaxis();
    mAxis[1] = hToProj->GetXaxis();
    mAxis[2] = hToProj->GetYaxis();
    mAxisName = "z";  mProjName = "l";
  }
    
  // Axis which would be projected
  mAxis[0]->SetRange();
  
  // Set ranges in which we would be projected first axis
  mAxis[1]->SetRangeUser(mProjRange[0], mProjRange[1]); 
  mAxis[2]->SetRangeUser(mProjRange[0], mProjRange[1]);

  // Make projection by the name
  TH1F* hProj = (TH1F*)hToProj->Project3D(mAxisName.c_str());

  // Setting title for you, my dear
  hProj->GetXaxis()->SetTitle(Form("q_{%s} (GeV/c)",mProjName.c_str()));
  hProj->GetYaxis()->SetTitle("C(q)");

  return hProj;
  
} //MakeProjection

//////////////////////////////////////////////////////
double FemtoFitter::LednickyWeigthCalc(double qInv) {


  int nsamples = 20000; // 10000 is enough for good precision;
  double r[4],rmag;
  double Rgauss[4], weight=0;

  //Source size in fm
  Rgauss[1]=mCoulRad; //out
  Rgauss[2]=mCoulRad; //side
  Rgauss[3]=mCoulRad; //long

  // Lednicky's weight calculation initialisation

  //initial parameters of model
  //  Bethe-Salpeter amplitude
  //   NS=1  Square well potential,
  //   NS=3  not used
  //   NS=4  scattered wave approximated by the spherical wave,
  //   NS=2  same as NS=4 but the approx. of equal emission times in PRF
  //         not required (t=0 approx. used in all other cases).
  FSINS.NS=4;
   
  // Switch for automatic setting of all parameters (0)
  // ITEST=1 any values of parameters ICH, IQS, ISI, I3C are allowed
  // ITEST=0 physical values of these parameters are put automatically
  LEDWEIGHT.ITEST=1;
  //  Swith for Couloumb interaction in the pair
  FSINS.ICH = 1;
  // Switches strong interactions
  FSINS.ISI = 0;
  // Switch for quantum statistics
  FSINS.IQS = 0;
  //Switches couloumb interaction with residual nucleus 
  FSINS.I3C = 0;
  //initial parameters of model
  LEDWEIGHT.IRANPOS=1;
 

  //---------------------------------------------------------------------
  //-   LL       1  2  3  4  5   6   7   8  9 10  11  12  13  14 15 16 17
  //-   part. 1: n  p  n  a  pi+ pi0 pi+ n  p pi+ pi+ pi+ pi- K+ K+ K+ K-
  //-   part. 2: n  p  p  a  pi- pi0 pi+ d  d  K-  K+  p   p  K- K+ p  p
  //   NS=1 y/n: +  +  +  +  +   -   -   -  -  -   -   -   -  -  -  -  -
  //----------------------------------------------------------------------
  //-   LL       18 19 20 21 22 23  24 25 26 27 28 29 30 31  32 
  //-   part. 1: d  d  t  t  K0 K0  d  p  p  p  n  /\ p  pi+ pi-
  //-   part. 2: d  a  t  a  K0 K0b t  t  a  /\ /\ /\ pb Xi- Xi-
  //   NS=1 y/n: -  -  -  -  -  -   -  -  -  +  +  +  -  -   -
  //----------------------------------------------------------------------
  int pdg1, pdg2;
  if (mPartType == 0) { FSINS.LL = 7; pdg1 = 211; pdg2 = 211; } //pions
  if (mPartType == 1) { FSINS.LL = 15; pdg1 = 321; pdg2 = 321; } //kaons
  
  TParticlePDG* tpart1 = TDatabasePDG::Instance()->GetParticle(pdg1);
  TParticlePDG* tpart2 = TDatabasePDG::Instance()->GetParticle(pdg2);

  FSIPOC.AM1 = tpart1->Mass(); //mass1
  FSIPOC.C1 = 1.;  //charge1
  FSIPOC.AM2 = tpart2->Mass(); //mass2
  FSIPOC.C2 = 1.;  //charge2

  fsiini_();
  
  double NumLed = 0.; 
  for (int isample = 0; isample < nsamples; isample++)
  {
    r[0]=0.0;
    for (int iProjection = 1; iProjection <= 3; iProjection++) // out, side, long
    {
      r[iProjection] = sqrt(2.0)*Rgauss[iProjection]*(gRandom->Gaus());
    } // iProjection
    rmag = sqrt(r[1]*r[1] + r[2]*r[2] + r[3]*r[3]); //MeV-1
       
    rmag = rmag/mHBar;

    // Values which needed for fsiw_();
    FSIPRF.PPX = 0;  
    FSIPRF.PPY = 0;  
    FSIPRF.PPZ = qInv*0.5;  
    FSIPRF.AK = qInv*0.5;  
    FSIPRF.AKS = qInv*qInv*0.25;  

    FSIPRF.X = r[1]/mHBar;  
    FSIPRF.Y = r[2]/mHBar;  
    FSIPRF.Z = r[3]/mHBar;
    FSIPRF.T = r[0]/mHBar;      
    FSIPRF.RP = rmag;  
    FSIPRF.RPS = rmag*rmag;  

    // Needed for weight
    fsiw_();

    //get weight from Lednicky's commons		
    weight = double(LEDWEIGHT.WEIN);
    NumLed += weight;

  } // isample    


  return NumLed/nsamples;

}

//////////////////////////////////////////////////////
double FemtoFitter::BowlerSinyukovFit(double* q, const double* par, int iOut, int iSide, int iLong) {

  // hQinv it is the hDen distibution with qInv weigth,
  // so here we simply get qInv
  double qInv = hQinv3D->GetBinContent(iOut, iSide, iLong);
  double qLow = hQinv3D->GetXaxis()->GetXmin();
  double qHi = hQinv3D->GetXaxis()->GetXmax(); 
  double qBins = hQinv3D->GetNbinsX();
  double qStep = (qHi-qLow)/qBins;
  int qCoulBin = (int)(qInv/qStep);
  
  double mCoul;
  if (mCoulombMethod == 0) mCoul = 1.;
  // Formula for this calculations was taken from:
  // Phys. Rev. C 97, 064912 (2018)
  if (mCoulombMethod == 1) mCoul = mKCoeffCauchSource[qCoulBin];
  // This values was taken from Lednicky codes:
  // Phys. Lett. B. 1998. V. 270. P. 69-74.
  // Phys. Lett. B. 1998. V. 432. P. 248-257.
  if (mCoulombMethod == 2) mCoul = mKCoeffSpherSource[qCoulBin];

  double mArg = 0.0;
  if (mSourceMethod == 0) //azimuthally-integrated HBT gaussian
  {
    mArg -= q[0]*q[0]*par[2]; //qo*qo*Ro^2
    mArg -= q[1]*q[1]*par[3]; //qs*qs*Rs^2
    mArg -= q[2]*q[2]*par[4]; //ql*ql*Rl^2
    mArg /= pow(mHBar, 2); // (hbar*c)^2 in (GeV*fm)^2
  } // mSourceMethod == 0
  
  if (mSourceMethod == 1) //azimuthally-integrated HBT lorentzian
  { //(I don't know is this work or not because of physics for 3d) 
    mArg -= q[0]*par[2]; //qo*Ro
    mArg -= q[1]*par[3]; //qs*s
    mArg -= q[2]*par[4]; //ql*Rl
    mArg /= pow(mHBar, 2); // (hbar*c)^2 in (GeV*fm)^2
    mArg /= 1./sqrt( M_PI );
  } // mSourceMethod == 1
  
  if (mSourceMethod == 2) //azimuthally-sesitive HBT
  {
    mArg -= q[0]*q[0]*par[2]; //qo*qo*Ro^2
    mArg -= q[1]*q[1]*par[3]; //qs*qs*Rs^2
    mArg -= q[2]*q[2]*par[4]; //ql*ql*Rl^2
    mArg -= 2.0*q[0]*q[1]*par[5]; //2*qo*qs*Ros^2
    mArg -= 2.0*q[0]*q[2]*par[6]; //2*qo*ql*Rol^2
    mArg -= 2.0*q[1]*q[2]*par[7]; //2*qs*ql*Rsl^2
    mArg /= pow(mHBar, 2); // (hbar*c)^2 in (GeV*fm)^2
  } // mSourceMethod == 2

  // Bowler-Sinyukov fit
  // Look to Phys. Lett. B 432 (1998) 248 and Phys. Lett. B 270 (1991) 69
  double mFit = par[0]*(1.0 - par[1] + par[1]*mCoul*(1.0 + exp(mArg)));
  return mFit;
  
} // BowlerSinyukovFit

//////////////////////////////////////////////////////
double FemtoFitter::BowlerSinyukov1DFit(const double* par, int iInv) {

  double qInv = hNum1D->GetBinCenter(iInv);
  double qLow = hNum1D->GetXaxis()->GetXmin();
  double qHi = hNum1D->GetXaxis()->GetXmax(); 
  double qBins = hNum1D->GetNbinsX();
  double qStep = (qHi-qLow)/qBins;
  int qCoulBin = (int)(qInv/qStep);

  double mCoul;
  if (mCoulombMethod == 0) mCoul = 1.;
  // Formula for this calculations was taken from:
  // Phys. Rev. C 97, 064912 (2018)
  if (mCoulombMethod == 1) mCoul = mKCoeffCauchSource[qCoulBin];
  // This values was taken from Lednicky codes:
  // Phys. Lett. B. 1998. V. 270. P. 69-74.
  // Phys. Lett. B. 1998. V. 432. P. 248-257.
  if (mCoulombMethod == 2) mCoul = mKCoeffSpherSource[qCoulBin];

  double mArg = 0.0;
  if (mSourceMethod == 0) //azimuthally-integrated HBT gaussian
  {
    mArg -= qInv*qInv*par[2]; //qinv*qinv*Rinv^2
    mArg /= pow(mHBar, 2); // (hbar*c)^2 in (GeV*fm)^2
  } // mSourceMethod == 0
  
  if (mSourceMethod == 1) //azimuthally-integrated HBT lorentzian
  {
    mArg -= qInv*par[2]; //qinv*Rinv
    mArg /= pow(mHBar, 2); // (hbar*c)^2 in (GeV*fm)^2
  } // mSourceMethod == 1
  
  // Bowler-Sinyukov fit
  // Look to Phys. Lett. B 432 (1998) 248 and Phys. Lett. B 270 (1991) 69
  double mFit = par[0]*(1.0 - par[1] + par[1]*mCoul*(1.0 + exp(mArg)));

  return mFit;
  
} // BowlerSinyukov1DFit

//////////////////////////////////////////////////////
void FemtoFitter::Set3DFitInitVal(double N, double lam,
                                  double Ro2, double Rs2, double Rl2,
                                  double Ros2, double Rol2, double Rsl2) {
  // Set initial values for minuit
  // This is a very non-trivial thing.
  // The quality of the fit depends on the initial parameters.
  // Be careful.
  m3DInitVal[0] = N;
  m3DInitVal[1] = lam;
  m3DInitVal[2] = Ro2;
  m3DInitVal[3] = Rs2;
  m3DInitVal[4] = Rl2;
  m3DInitVal[5] = Ros2;
  m3DInitVal[6] = Rol2;
  m3DInitVal[7] = Rsl2;

} // Set3DFitInitVal

//////////////////////////////////////////////////////
void FemtoFitter::Set1DFitInitVal(double N, double lam,
                                  double Rinv ) {
  // Set initial values for minuit
  // This is a very non-trivial thing.
  // The quality of the fit depends on the initial parameters.
  // Be careful.
  m1DInitVal[0] = N;
  m1DInitVal[1] = lam;
  m1DInitVal[2] = Rinv;

} // Set1DFitInitVal

//////////////////////////////////////////////////////
void FemtoFitter::Set3DFitInitValErr(double N, double lam,
                                     double Ro2, double Rs2, double Rl2,
                                     double Ros2, double Rol2, double Rsl2) {
  // It's also very important parameters,
  // especially if you fit in the small range.
  m3DInitValErr[0] = N;
  m3DInitValErr[1] = lam;
  m3DInitValErr[2] = Ro2;
  m3DInitValErr[3] = Rs2;
  m3DInitValErr[4] = Rl2;
  m3DInitValErr[5] = Ros2;
  m3DInitValErr[6] = Rol2;
  m3DInitValErr[7] = Rsl2;

} // Set3DFitInitValErr

//////////////////////////////////////////////////////
void FemtoFitter::Set1DFitInitValErr(double N, double lam,
                                     double Rinv ) {
  // It's also very important parameters,
  // especially if you fit in the small range.
  m1DInitValErr[0] = N;
  m1DInitValErr[1] = lam;
  m1DInitValErr[2] = Rinv;

} // Set1DFitInitValErr

//////////////////////////////////////////////////////
void FemtoFitter::SetFitParameters(double FitRangeLo, double FitRangeHi,
                                   double ProjRangeLo, double ProjRangeHi) {
  // Set fit range (may affect parameters)
  mFitRange[0] = FitRangeLo;
  mFitRange[1] = FitRangeHi;

  // Set projections range (It is customary to take a range near the peak - |0.05|)
  mProjRange[0] = ProjRangeLo;
  mProjRange[1] = ProjRangeHi;
  
} // SetFitParameters

//////////////////////////////////////////////////////
void FemtoFitter::SetParticleType(int PartType) {

  if (PartType < 0 || PartType > 2)
  {
    cout << "//////////////////////////////////////////////////////" << endl;
    cout << "We have two particle type:" << endl;
    cout << "0 - pions" << endl;
    cout << "1 - kaons" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mPartType = PartType;
  
}

//////////////////////////////////////////////////////
void FemtoFitter::SetMassOfParticle(double MassPart) {
  // You should set mass of particle, which you analyzing for Coulomb corrections
  // In GeV
  if (MassPart < 0)
  {
    cout << "//////////////////////////////////////////////////////" << endl;
    cout << "HiHi!! You use negative value for mass value." << endl;
    cout << "I AM THE LAW" << endl;
    cout << "Don't do this, please ^_^" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
    throw exception();
  }
  mMassPart = MassPart;
  
} //SetMassOfParticle

//////////////////////////////////////////////////////
void FemtoFitter::SetCoulombCalcMethod(int CoulombMethod) {
  // Here you can choose method of calculation coulomb correction value
  // 0 - no correction applied
  // 1 - Coulomb corrections calculating by the CoulombCauchySource
  // 2 - Coulomb corrections calculating by the CoulombSphericalSource
  if (CoulombMethod < 0 || CoulombMethod > 2)
  {
    cout << "//////////////////////////////////////////////////////" << endl;
    cout << "Hi! Value for Coulomb method calculations is incorrect. Your value should be 0, 1 or 2." << endl;
    cout << "0 - no correction will be applied" << endl;
    cout << "1 - Coulomb corrections calculating by the CoulombCauchySource" << endl;
    cout << "2 - Coulomb corrections calculating by the CoulombSphericalSource" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
    throw exception();
  }
  
  mCoulombMethod = CoulombMethod;
  
} // SetCoulombCalcMethod

//////////////////////////////////////////////////////
void FemtoFitter::SetFormOfSource(int SourceMethod) {

  if (SourceMethod < 0 || SourceMethod > 2)
  {
    cout << "//////////////////////////////////////////////////////" << endl;
    cout << "Hi! Value for form of the emission source is incorrect. Your value should be 0, 1 or 2." << endl;
    cout << "0 - Gaussian with cross-term parameters equals to zero (azimuthally-integrated HBT)" << endl;
    cout << "1 - Lorenzian with cross-term parameters equals to zero " << endl;
    cout << "2 - Gaussian (azimuthally-sensitive HBT)" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
    throw exception();
  }
  mSourceMethod = SourceMethod;
} // SetFormOfSource

//////////////////////////////////////////////////////
void FemtoFitter::SetCoulombRadius(double CoulRad) {
  // Here we set Coulomb radius which need to Coulomb factor calculations
  mCoulRad = CoulRad;

} // SetCoulombRadius

//////////////////////////////////////////////////////
void FemtoFitter::Set3DHistoForCf(TH3F* hNum3DFile, TH3F* hDen3DFile, TH3F* hQinv3DFile) {

  if (!hNum3DFile || !hDen3DFile)
  {
    cout << "//////////////////////////////////////////////////////" << endl;
    cout << "Why, hello my friend!" << endl;
    cout << "Your histos in 'Set3DHistoForCf' is empty or something else here is wrong. So we finished here to avoid big scary segfault." << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
    throw exception();
  }
  if (!hQinv3DFile)
  {
    cout << "//////////////////////////////////////////////////////" << endl;
    cout << "Heeeey, if you do not set qInv distribution - you can not get coulomb correction" << endl;
    cout << "Sorry:(" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }

  hNum3D = (TH3F*)hNum3DFile->Clone("hNum3D");
  hDen3D = (TH3F*)hDen3DFile->Clone("hDen3D");
  hQinv3D = (TH3F*)hQinv3DFile->Clone("hDen3D");
  
} // SetHistoForCf

//////////////////////////////////////////////////////
void FemtoFitter::Set1DHistoForCf(TH1F* hNum1DFile, TH1F* hDen1DFile) {

  if (!hNum1DFile || !hDen1DFile)
  {
    cout << "//////////////////////////////////////////////////////" << endl;
    cout << "Why, hello my friend!" << endl;
    cout << "Your histos in 'Set1DHistoForCf' is empty or something else here is wrong. So we finished here to avoid big scary segfault." << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
    throw exception();
  }

  hNum1D = (TH1F*)hNum1DFile->Clone("hNum1D");
  hDen1D = (TH1F*)hDen1DFile->Clone("hDen1D");
  
} // SetHistoForCf


//////////////////////////////////////////////////////
void FemtoFitter::SetFitterCustomize(bool FitCust) {
  mFitterCustomize = FitCust;
} 


//////////////////////////////////////////////////////
void FemtoFitter::SetScaleForErrors(double ErrDef) {
  if(ErrDef == -99)
  {
    cout << "////////////////////////SetScaleForErrors////////////////////////" << endl;
    cout << "Here you could add scale to minuit output errors." << endl;
    cout << "Usually if you use chi2 method scale = 1, if log-likelihood = 0.5" << endl;
    cout << "You should be careful. You can add this scale here or directly to your formula." << endl;
    cout << "Please read: http://seal.web.cern.ch/seal/documents/minuit/mnerror.pdf. (P. 3)" << endl;
    cout << "or" << endl;
    cout << "Please read: https://root.cern.ch/root/htmldoc/guides/minuit2/Minuit2Letter.pdf (P. 12)" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mErrDef = ErrDef;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetMaxFunctionCalls(int MaxCalls) {
  if (MaxCalls == -99)
  {
    cout << "////////////////////////SetMaxFunctionCalls////////////////////////" << endl;
    cout << "The optional argument specifies the (approximate) maximum number " << endl;
    cout << "of function calls after which the calculation will be stopped even " << endl;
    cout << "if it has not yet converged." << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mMaxCalls = MaxCalls;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetPrecision(double Precision) {
  if (Precision == -99)
  {
    cout << "////////////////////////SetPrecision////////////////////////" << endl;
    cout << "Well... Using of this method are risky. Please read this:" << endl;
    cout << "https://root-forum.cern.ch/t/use-of-the-precision-in-minuit/42556/7 " << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mPrecision = Precision;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetDistanceFromMinimum(double Tolerance) {
  if (Tolerance == -99)
  {
    cout << "////////////////////////SetDistanceFromMinimum////////////////////////" << endl;
    cout << "The optional argument tolerance specifies the required tolerance" << endl;
    cout << "on the function value at the minimum. The default tolerance value is 0.1," << endl;
    cout << "and the minimization will stop when the estimated vertical distance to the" << endl;
    cout << "minimum (EDM) is less than 0.001∗tolerance∗ErrDef" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mTolerance = Tolerance;
}

//////////////////////////////////////////////////////
void FemtoFitter::SetStrategy(int Strategy) {
  if (Strategy == -99)
  {
    cout << "////////////////////////SetStrategy////////////////////////" << endl;
    cout << "Parameter can take on 0, 1(default), 2. Value 0 indicates to Minuit " << endl;
    cout << "that it should economize function calls; Value 2 indicates that Minuit " << endl;
    cout << "is allowed to waste function calls in order to be sure thatall values are precise;" << endl;
    cout << " Please read: https://root.cern.ch/download/minuit.pdf (P. 3)" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mStrategy = Strategy;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetValidError(bool ValidError) {
  //flag to check if minimizer needs to perform accurate error analysis (e.g. run Hesse for Minuit)
  mValidError = ValidError;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetPrintLevel(int PrintLevel) {
  if(PrintLevel == -99)
  {
    cout << "////////////////////////SetPrintLevel////////////////////////" << endl;
    cout << "Print different number of information: -1 - suppress all info," << endl;
    cout << "0 - print errors, 1... just try it ;) " << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mPrintLevel = PrintLevel;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetFixedVariable(vector<int> parFixNumber, vector<double> fixVar) {
  if (parFixNumber.at(0) == -99) {
    cout << "////////////////////////SetFixedVariable////////////////////////" << endl;
    cout << "Here you could fix some parameters, you need to set parameter number " << endl;
    cout << "and value wich you want to fix. Here some help for you:" << endl;
    cout << "3D: 0-Norm, 1-lambda, 2-Ro2, 3-Rs2, 4-Rl2, 5-Ros2, 6-Rol2, 7-Rsl2" << endl;
    cout << "1D: 0-Norm, 1-lambda, 2-Rinv" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mParFixNumber = parFixNumber;
  mFixVar = fixVar;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetVariableLimits(vector<int> parLimNumber, vector<double> limVarLo, vector<double> limVarHi) {
  if (parLimNumber.at(0) == -99) {
    cout << "////////////////////////SetFixedVariable////////////////////////" << endl;
    cout << "Here you could set limits to some parameters, you need to set parameter number " << endl;
    cout << "and lo, hi values. Here some help for you:" << endl;
    cout << "3D: 0-Norm, 1-lambda, 2-Ro2, 3-Rs2, 4-Rl2, 5-Ros2, 6-Rol2, 7-Rsl2" << endl;
    cout << "1D: 0-Norm, 1-lambda, 2-Rinv" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mParLimNumber = parLimNumber;
  mLimVarLo = limVarLo;
  mLimVarHi = limVarHi;
}


//////////////////////////////////////////////////////
void FemtoFitter::SetVariableStepSize(vector<int> parStepNumber, vector<double> stepVar) {
  if (parStepNumber.at(0) == -99) {
    cout << "////////////////////////SetFixedVariable////////////////////////" << endl;
    cout << "Here you could set step size for parameters, you need to set parameter number " << endl;
    cout << "and value wich you want to fix. Here some help for you:" << endl;
    cout << "3D: 0-Norm, 1-lambda, 2-Ro2, 3-Rs2, 4-Rl2, 5-Ros2, 6-Rol2, 7-Rsl2" << endl;
    cout << "1D: 0-Norm, 1-lambda, 2-Rinv" << endl;
    cout << "//////////////////////////////////////////////////////" << endl;
  }
  mParStepNumber = parStepNumber;
  mStepVar = stepVar;
}
