//////////////////////////////////////////////////////
//
// Hello, there! This class is written so that you
// can fit correlation functions in femtoscopic
// analysis.
// If you have any questions please contact me:
// eugenia.sh.el@gmail.com
//
//////////////////////////////////////////////////////

#ifndef FemtoFitter_h
#define FemtoFitter_h

// c++ includes
#include <vector>
#include <exception>
#include <iostream>
#include <cmath>

// ROOT includes
#include <TH1.h>
#include <TH3.h>
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include <TRandom2.h> 
#include <TDatabasePDG.h>
#include <TParticlePDG.h>

// Lednicky Coulomb includes
#include "WLedCOMMONS.h"

using namespace std;
using namespace ROOT::Math;

// Extern for Lednicky Coulomb calculations
extern "C" void   fsiini_();
extern "C" void   fsiw_();
extern LEDWEIGHTCommon LEDWEIGHT;
extern FSINSCommon FSINS;
extern FSIPOCCommon FSIPOC;
extern FSIPRFCommon FSIPRF;

//plank constant eV*muM
const double mHBar = 0.1973269804;

// alpha is the fine-structure constant
double alpha = 0.00729735256;

//////////////////////////////////////////////////////
class FemtoFitter {

public:
  
  FemtoFitter();
  ~FemtoFitter();
  
  static TH3F* hNum3D;
  static TH3F* hDen3D;
  static TH3F* hQinv3D;

  static TH1F* hNum1D;
  static TH1F* hDen1D;
  
  //Fit parameters
  //8 fit parameters (Norm + lambda + 3radii + 3cross-comp.)
  static const int m3DNumPar = 8; 
  static const int m3DNumParInt = 5; 
  double m3DInitVal[m3DNumPar];
  double m3DInitValErr[m3DNumPar];
  
  //3 fit parameters (Norm + lambda + inv. rad.)
  static const int m1DNumPar = 3; 
  double m1DInitVal[m1DNumPar];
  double m1DInitValErr[m1DNumPar];
  
  static double mFitRange[2]; //lo, hi
  double mProjRange[2]; //lo, hi
  static int mSourceMethod;
  static int mCoulombMethod;
  static double mCoulRad;
  static double mMassPart;
  static int mPartType;
  static vector<double> mKCoeffSpherSource;
  static vector<double> mKCoeffCauchSource;
  bool mFitterCustomize;
  double mErrDef;
  int mMaxCalls;
  double mPrecision;
  double mTolerance;
  int mStrategy;
  bool mValidError;
  vector<int> mParFixNumber; vector<double> mFixVar;
  vector<int> mParLimNumber; vector<double> mLimVarLo; vector<double> mLimVarHi;
  vector<int> mParStepNumber; vector<double> mStepVar;
  int mPrintLevel;
  
  vector<double> mGet3DFitPar;
  vector<double> mGet3DFitParErr;
  vector<double> mGet1DFitPar;
  vector<double> mGet1DFitParErr;
  vector<TH1F*> h3DCfProj; // 3 projection - out, side, long
  vector<TH1F*> h3DCfProjFit; // 3 projection - out, side, long
  vector<TH1F*> h1DCf;
  vector<TH1F*> h1DCfFit;
  
  double m3DChi2PerNdf;
  double m1DChi2PerNdf;

  void Clear();
  void DoFit3D();
  void DoFit1D();
  TH1F* MakeProjection(int osl, TH3F* hToProj);
  double LednickyWeigthCalc(double qInv);
  void CalculateCoulombCorrection(TAxis *xAxis);
  static double BowlerSinyukovFit(double* x, const double* param, int iOut, int iSide, int iLong); 
  static double BowlerSinyukov1DFit(const double* param, int iInv); 
  static double Minimization3DLogLike(const double *par);
  static double Minimization1DLogLike(const double *par);
  
  ////////////////////////////////////////
  //Setters
  ////////////////////////////////////////
  void Set3DFitInitVal(double N = 1., double lam = 0.6,
                       double Ro2 = 2., double Rs2 = 2., double Rl2 = 2.,
                       double Ros2 = 0., double Rol2 = 0., double Rsl2 = 0.); 
  void Set3DFitInitValErr(double N = 0.01, double lam = 0.05,
                          double Ro2 = 0.01, double Rs2 = 0.01, double Rl2 = 0.01,
                          double Ros2 = 0.01, double Rol2 = 0.01, double Rsl2 = 0.01);
  void Set1DFitInitVal(double N = 1., double lam = 0.6, double Rinv = 2.); 
  void Set1DFitInitValErr(double N = 0.01, double lam = 0.05, double Rinv = 0.01);
  void SetFitParameters(double FitRangeLo = -0.4, double FitRangeHi = 0.4,
                        double ProjRangeLo = -0.05, double ProjRangeHi = 0.05);
  void Set3DHistoForCf(TH3F* hNumFile = 0, TH3F* hDenFile = 0, TH3F* hQinvFile = 0);
  void Set1DHistoForCf(TH1F* hNum1DFile = 0, TH1F* hDen1DFile = 0);
  void SetCoulombCalcMethod(int CoulombMethod);
  void SetCoulombRadius(double CoulRad);
  void SetMassOfParticle(double MassPart);
  void SetParticleType(int PartType);
  void SetFormOfSource(int SourceMethod);

  void SetFitterCustomize(bool FitCust);
  void SetScaleForErrors(double ErrDef);
  void SetMaxFunctionCalls(int MaxCalls);
  void SetPrecision(double Precision);
  void SetDistanceFromMinimum(double Tolerance);
  void SetStrategy(int Strategy);
  void SetValidError(bool ValidError);
  void SetFixedVariable(vector<int> parFixNumber, vector<double> fixVar);
  void SetVariableLimits(vector<int> parLimNumber, vector<double> limVarLo, vector<double> limVarHi);
  void SetVariableStepSize(vector<int> parStepNumber, vector<double> stepVar);
  void SetPrintLevel(int PrintLevel);
  
  ////////////////////////////////////////
  //Getters
  ////////////////////////////////////////
  vector<double> Get3DFitParameters() {return mGet3DFitPar;};
  vector<double> Get3DFitParametersErr() {return mGet3DFitParErr;};
  vector<double> Get1DFitParameters() {return mGet1DFitPar;};
  vector<double> Get1DFitParametersErr() {return mGet1DFitParErr;};
  vector<TH1F*> Get3DCfProj() {return h3DCfProj;};
  vector<TH1F*> Get3DCfProjFit() {return h3DCfProjFit;};
  vector<TH1F*> Get1DCf() {return h1DCf;};
  vector<TH1F*> Get1DFit() {return h1DCfFit;};
  double Get3DChi2PerNdf() {return m3DChi2PerNdf;};
  double Get1DChi2PerNdf() {return m1DChi2PerNdf;};
};

TH3F  *FemtoFitter::hNum3D = 0;
TH3F  *FemtoFitter::hDen3D = 0;
TH3F  *FemtoFitter::hQinv3D = 0;
TH1F  *FemtoFitter::hNum1D = 0;
TH1F  *FemtoFitter::hDen1D = 0;
double FemtoFitter::mFitRange[2] = {0};
int    FemtoFitter::mCoulombMethod = -999;
int    FemtoFitter::mSourceMethod = -999;
double FemtoFitter::mCoulRad = -999.;
double FemtoFitter::mMassPart = -999.;
int    FemtoFitter::mPartType = -999;
vector<double> FemtoFitter::mKCoeffSpherSource;
vector<double> FemtoFitter::mKCoeffCauchSource;

#endif //#define FemtoFitter_h
