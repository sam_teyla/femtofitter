#ifndef WLedCOMMON
#define WLedCOMMON

extern "C" {

#define f2cFortran
#include "cfortran.h"



//----------------------------------------------------------------
/*
 COMMON/CONS/PI,PI2,SPI,DR,W   
*/
	            
 typedef struct //FSI_CONS
   {
   double PI;  // 3.141592654 
   double PI2; // PI2=2*PI 
   double SPI; // SPI=DSQRT(PI)  
   double DR;  // DR=180.D0/PI  from radian to degree  
   double W;   // W=1/.1973D0    from fm to 1/GeV 
  }FSICONSCommon;
 
#define FSICONS COMMON_BLOCK(FSICONS,fsicons)
COMMON_BLOCK_DEF(FSICONSCommon, FSICONS);
//----------------------------------------------------------------
//   COMMON/LEDWEIGHT/WEIF,WEI,WEIN,ITEST,IRANPOS  
   
	            
 typedef struct //LEDWEIGHT
   {
   double WEIF;  
   double WEI;   
   double WEIN;  
   int ITEST;
   int IRANPOS;
  }LEDWEIGHTCommon;
 
#define LEDWEIGHT COMMON_BLOCK(LEDWEIGHT,ledweight)
COMMON_BLOCK_DEF(LEDWEIGHTCommon, LEDWEIGHT);

//----------------------------------------------------------------- 
//---------------------------------------------------------------------------       
//             COMMON/FSI_NS/LL,NS,ICH,ISI,IQS,I3C,I3S 
//        INTEGER LL,NS,ICH,ISI,IQS,I3C,I3S

                                  
      typedef struct //FSI_NS                                                      
         {                                                                           
	int LL; // [GeV/c]                                                     
	int NS;                                                                
	int ICH;                                                                
	int ISI;                                                                 
	int IQS;                                                                 
	int I3C;                                                                
	int I3S;
	}FSINSCommon;                                                       
			                                                                                      
#define FSINS COMMON_BLOCK(FSINS,fsins)                                  
COMMON_BLOCK_DEF(FSINSCommon, FSINS);                               


//---------------------------------------------------------------------------  
//     COMMON/FSI_POC/AMN,AM1,AM2,CN,C1,C2,AC1,AC2                             
                                                                            
 typedef struct //FSI_POC                                                      
    {                                                                           
      double AMN; //mass of the effective nucleus   [GeV/c**2]                   
      double AM1;                                                                
      double AM2;                                                                
      double CN; //charge of the effective nucleus [elem. charge units]          
      double C1;                                                                 
      double C2;                                                                 
      double AC1;                                                                
      double AC2;                                                                
    }FSIPOCCommon;                                                       
		                                                                                     
#define FSIPOC COMMON_BLOCK(FSIPOC, fsipoc)                                  
COMMON_BLOCK_DEF(FSIPOCCommon, FSIPOC);                               
		                                                                                     

//----------------------------------------------------------------- 
/*       COMMON/MOMLAB/AM1,PXP1,PYP1,PZP1,AM2,PXP2,PYP2,PZP2                          
               REAL*8 AM1,PXP1,PYP1,PZP1,AM2,PXP2,PYP2,PZP2                                 
*/	                                                                                            
	            

 typedef struct //MOMLAB
   {
   double AM1;
   double PXP1;
   double PYP1;
   double PZP1;
   double AM2;
   double PXP2;
   double PYP2;
   double PZP2;
   
  }MOMLABCommon;
 
#define MOMLAB COMMON_BLOCK(MOMLAB,momlab)

COMMON_BLOCK_DEF(MOMLABCommon, MOMLAB);

//---------------------------------------------------------------------------  
/*           COMMON/FSI_MOM/P1X,P1Y,P1Z,E1,P1,  ! particle momenta in the      
//                                               rest frame of effective nucleu
     1       P2X,P2Y,P2Z,E2,P2                                                 
     */                                                                             
      typedef struct //FSI_MOM                                                      
         {                                                                           
	double P1X; // [GeV/c]                                                     
	double P1Y;                                                                
	double P1Z;                                                                
	double E1;                                                                 
	double P1;                                                                 
	double P2X;                                                                
	double P2Y;                                                                
	double P2Z;                                                                
	double E2;                                                                 
	double P2;                                                                 
	}FSIMOMCommon;                                                       
			                                                                                      
#define FSIMOM COMMON_BLOCK(FSIMOM,fsimom)                                  
COMMON_BLOCK_DEF(FSIMOMCommon, FSIMOM);                               
			                                                                                      
//-----------------------------------------------------------------------                                                                                     
                                                                               
 typedef struct //FSI_COOR                                                     
    {                                                                           
    double X1;                                                                 
    double Y1;                                                                 
    double Z1;                                                                 
    double T1;                                                                 
    double R1;                                                                 
    double X2;                                                                 
    double Y2;                                                                 
    double Z2;                                                                 
    double T2;                                                                 
    double R2;                                                                 
    }FSICOORCommon;                                                      
			                                                                                 
#define FSICOOR COMMON_BLOCK(FSICOOR,fsicoor)                               
COMMON_BLOCK_DEF(FSICOORCommon, FSICOOR);                             


//---------------------------------------------------------------------------         
                                                                                      
/*     COMMON/FSI_PRF/PPX,PPY,PPZ,AK,AKS, ! k*=(p1-p2)/2 and x1-x2                    
                     X,Y,Z,T,RP,RPS      ! in pair rest frame (PRF)                   
		     */                                                                                    
		                                                                                           
typedef struct //FSI_PRF                                                             
 {                                                                                  
  double PPX;                                                                       
  double PPY;                                                                       
  double PPZ;                                                                       
  double AK;                                                                        
  double AKS;                                                                       
  double X;                                                                         
  double Y;                                                                         
  double Z;                                                                         
  double T;                                                                         
  double RP;                                                                        
  double RPS;                                                                       
  }FSIPRFCommon;                                                              
						                                                                                       
#define FSIPRF COMMON_BLOCK(FSIPRF,fsiprf)                                         
COMMON_BLOCK_DEF(FSIPRFCommon, FSIPRF);                                      


			                                                                                      
 
}
#endif                     
