//////////////////////////////////////////////////////
//
// Hi, there!
// This macro is needed for the FemtoFitter class
// If you have any questions please contact me:
// eugenia.sh.el@gmail.com
//
//////////////////////////////////////////////////////
R__LOAD_LIBRARY(FemtoFitter/FemtoFitterLibrary)
#include "FemtoFitter/FemtoFitter.h"

#define KT_BIN 1//4
#define CENT_BIN 1//5
#define PAR_BIN 4

TFile *fileOut;
TH1F  *hCF[CENT_BIN][KT_BIN],  *hFit[CENT_BIN][KT_BIN];
double  mParam[PAR_BIN][CENT_BIN][KT_BIN], mParamErr[PAR_BIN][CENT_BIN][KT_BIN];
TGraphErrors *hPar[PAR_BIN][CENT_BIN];
int CentLo, CentHi, KtLo, KtHi;
vector<double> mKtAxis, mKtAxisErr;
const char *titles[] = {"Norm", "#lambda", "R_{inv}", "#chi^{2}/NDF"};

void RunFemtoFitter1D(const char* fIn = //"afterSumBinsAuAu.root",
"afterSumBins_smallSyst1D.root",
                      const char *fOut = "outFemtoFitter1D.root")
{

  TFile *fileIn = new TFile(fIn, "READ");
  fileOut = new TFile(fOut, "RECREATE");
  
  for (int iCent = 0; iCent < CENT_BIN; iCent++) {
    for (int iKt = 0; iKt < KT_BIN; iKt++) {

      CentLo = iCent; CentHi = iCent;
       // if (iKt == 3) { KtLo = 3; KtHi = 4; }
       // else {
        KtLo = iKt; KtHi = iKt;
        // }
 
      // Read numenators and denumenators from input file  
      // const char *nameNum = Form("num_ch_0_1_cent_%i_%i_kt_%i_%i",CentLo,CentHi,KtLo,KtHi);
      // const char *nameDen = Form("den_ch_0_1_cent_%i_%i_kt_%i_%i",CentLo,CentHi,KtLo,KtHi);
      
      const char *nameNum = Form("num_ch_0_1_cent_%i_%i_eta_0_0_syst_0_0_kt_%i_%i",CentLo,CentHi,KtLo,KtHi);
      const char *nameDen = Form("den_ch_0_1_cent_%i_%i_eta_0_0_syst_0_0_kt_%i_%i",CentLo,CentHi,KtLo,KtHi);

      TH1F *hNum_sum = (TH1F *)fileIn->Get(nameNum);
      if (!hNum_sum) cout << "Something wrong with " << nameNum << endl;
      TH1F *hDen_sum = (TH1F *)fileIn->Get(nameDen);
      if (!hDen_sum) cout << "Something wrong with " << nameDen << endl;

      // Create new fitter
      FemtoFitter *fitter = new FemtoFitter();

      fitter->Set1DHistoForCf(hNum_sum, hDen_sum);

      // If thue - you could customize fitter parameters (needed for difficult cases)
      // print -99 to call info about methods
      fitter->SetFitterCustomize(false);
      fitter->SetScaleForErrors(1); // error definition (=1. for getting 1 sigma error for chi2 fits)
      fitter->SetMaxFunctionCalls(2000); // maximum number of function calls
      fitter->SetPrecision(0);  // prec. of the objective function evaluation (val. <=0 means default)
      fitter->SetDistanceFromMinimum(0.1); // minimize tolerance to reach solution
      fitter->SetStrategy(1); // minimizer strategy 
      fitter->SetValidError(false);
      // if you want to check how you theory work with fixed parameters
      vector <int> numVarFix = {5, 6, 7}; // number of parameter (if first parameter =-99 you call help print out)
      vector <double> valVarFix = {0.,0.,0.}; // fixed values 
      fitter->SetFixedVariable(numVarFix, valVarFix);
      // if you want to set limits to parameters
      vector <int> numVarLim = {0, 1, 2, 3, 4, 5, 6, 7}; // number of parameter (if first parameter =-99 you call help print out)
      vector <double> valVarLimLo = {0.,0., 0.,0.,0., -2.,-2.,-2.}; // lower limit values
      vector <double> valVarLimHi = {1.,1., 60.,60.,60., 2.,2.,2.}; // higher limit values
      fitter->SetVariableLimits(numVarLim, valVarLimLo, valVarLimHi);
      // if you need to change step which uses for minuit iterations
      vector <int> numVarStep = {0, 1, 2, 3, 4, 5, 6, 7};
      vector <double> valVarStep = {0.2,0.2,0.2, 0.2,0.2,0.2, 0.2,0.2};
      fitter->SetVariableStepSize(numVarStep, valVarStep);
      
      fitter->SetPrintLevel(1); //print different level of informations

      // Set initial values for minuit. The quality of the fit depends on the initial parameters.
      fitter->Set1DFitInitVal(0.05, 0.1, 2. );     // norm, lambda, Rinv^2
                              
      // It's also very important parameters, especially if you fit in the small range.
      fitter->Set1DFitInitValErr(0.001, 0.001, 0.001);     // norm, lambda, Rinv^2

      // Set fit range (may affect parameters)
      // Set projections range (it is customary to take a range near the peak - |0.05|)
      fitter->SetFitParameters(0.0, 2.0, -0.05, 0.05);     // FitRangeLo,  FitRangeHi
      
      // Here you can choose the form of the emission source (if you want to see options set here some -99) 
      fitter->SetFormOfSource(0);
      
      // If you want have Coulomb correction factor (if you want to see options set here some -99)
      fitter->SetCoulombCalcMethod(0);
      
      // If you want to use Coulomb, you should set Coulom radius (fm)
      fitter->SetCoulombRadius(5);

      // If you want to use Coulomb, you should set Mass of analizing particles in GeV
      fitter->SetMassOfParticle(0.139);
      fitter->SetParticleType(0);
      
      // Perform fitting of the selected parameters and histograms.
      fitter->DoFit1D();

      // Get vectors of cf and fit 
      vector<TH1F*> hCf = fitter->Get1DCf();
      vector<TH1F*> hFits = fitter->Get1DFit();

      // Get projections from the vector and write it to the output file.
      hCF[iCent][iKt] = hCf[0];
      hCF[iCent][iKt]->SetName(Form("Rinv_CF_%i_%i", iCent,iKt));
      hCF[iCent][iKt]->SetMarkerStyle(kFullCircle);
      hCF[iCent][iKt]->SetMarkerColor(kBlack);
      hCF[iCent][iKt]->SetLineColor(kBlack);
      hCF[iCent][iKt]->Write();
        
      hFit[iCent][iKt] = hFits[0];
      hFit[iCent][iKt]->SetName(Form("Rinv_Fit_%i_%i", iCent,iKt));
      hFit[iCent][iKt]->SetLineColor(kRed);
      hFit[iCent][iKt]->Write();
        
      
      vector<double> mPar = fitter->Get1DFitParameters();
      vector<double> mParErr = fitter->Get1DFitParametersErr();
      double mChi2 = fitter->Get1DChi2PerNdf();

      for (int iPar = 0; iPar < PAR_BIN; iPar++) {
        if (iPar == 2) {
          mParam[iPar][iCent][iKt] = sqrt(mPar[iPar]);
          mParamErr[iPar][iCent][iKt] = mParErr[iPar]/(2.*sqrt(mPar[iPar]));
        }
        if (iPar == 3) {
          mParam[iPar][iCent][iKt] = mChi2;
          mParamErr[iPar][iCent][iKt] = 0.;
        }
        else {
          mParam[iPar][iCent][iKt] = mPar[iPar];
          mParamErr[iPar][iCent][iKt] = mParErr[iPar];
        }
      } // iPar
      if (iKt == 3) { mKtAxis.push_back(0.525); }
      else { mKtAxis.push_back(0.2 + iKt*0.1); }
      mKtAxisErr.push_back(0.01);
      
    } ////  iKt
    
    for (int iPar = 0; iPar < PAR_BIN; iPar++) {
      hPar[iPar][iCent] = new TGraphErrors(KT_BIN, mKtAxis.data(), mParam[iPar][iCent], mKtAxisErr.data(), mParamErr[iPar][iCent]);
      hPar[iPar][iCent]->SetName(Form("Par_%i_%i", iPar,iCent));
      hPar[iPar][iCent]->GetYaxis()->SetTitle(Form("%s", titles[iPar]));
      hPar[iPar][iCent]->GetXaxis()->SetTitle("k_{T} (GeV/c)");
      hPar[iPar][iCent]->Write();
    } // iPar
    
  } // iCent

  fileOut->Close();
  fileIn->Close();

} // RunFemtoFitter
